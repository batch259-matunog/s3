<?php require_once './code.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>S03 - Access Modifiers and Encapsulation</title>
</head>
<body>
  <h1>Activity 2</h1>

  <h2>Building</h2>
  <p>The name of the building is <?php echo $building->name ?>.</p>
  <p>The <?php echo $building->name ?> has <?php echo $building->getFloor(); ?> floors.</p>
  <p>The <?php echo $building->name ?> is located at <?php echo $building->getAddress(); ?>.</p>
  <p>The name of the building has been changed to <?php echo $building->name = 'Caswynn Complex'; ?>.</p>

  <h2>Condominium</h2>
  <p>The name of the condominium is <?php echo $condominium->name ?>.</p>
  <p>The <?php echo $condominium->name ?> has <?php echo $condominium->getFloor(); ?> floors.</p>
  <p>The <?php echo $condominium->name ?> is located at <?php echo $condominium->getAddress(); ?>.</p>
  <p>The name of the condominium has been changed to <?php echo $condominium->name = 'Enzo Tower'; ?>.</p>
  
</body>
</html>