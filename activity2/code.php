<?php

class Building {

  public $name;
  protected $floor;
  protected $address;

  public function __construct($name, $floor, $address){
    $this->name = $name;
    $this->floor = $floor;
    $this->address = $address;
  }

  // floor getter
  public function getFloor(){
    return $this->floor;
  }
  // address getter
  public function getAddress(){
    return $this->address;
  }
  // floor setter
  public function setFloor($floor){
    $this->floor = $floor;
  }
  // address setter
  public function setAddress($address){
    $this->address = $address;
  }

};

class Condominium extends Building {

};

$building = new Building(
  'Caswynn Building',
  8,
  'Timog Avenue, Quezon City, Philippines'
);

$condominium = new Condominium(
  'Enzo Condo', 
  5, 
  'Buendia Avenue, Makati City, Philippines'
);

?>